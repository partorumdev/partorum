from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.db import transaction

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from django.http import JsonResponse

from django.contrib.auth.models import User
from . import models
from .clipping import *


class ClippingSave():
	model = models.Recorte
	fields = ['texto', 'autor', 'livro', 'posicao']
	template_name = 'create_clipping.html'
	success_url = reverse_lazy('core:home')


class ClippingCreate(LoginRequiredMixin, ClippingSave, CreateView):

	def form_valid(self, form):
		clipping = form.save(commit=False)
		clipping.user = self.request.user
		clipping.save()
		return super(ClippingSave, self).form_valid(form)

class ClippingUpdate(LoginRequiredMixin, ClippingSave, UpdateView):
	pass

class ClippingDelete(LoginRequiredMixin, DeleteView):
    model = models.Recorte
    success_url = reverse_lazy('core:home')
    template_name = 'clipping_confirm_delete.html'

@method_decorator(cache_page(60 * 5), name='dispatch')
class ClippingListView(LoginRequiredMixin, ListView):

	template_name = 'list_clipping.html'
	model = models.Recorte
	cache_timeout = 60

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['user'] = self.request.user
		return context

	def get_queryset(self):
		queryset = models.Recorte.objects.filter(user=self.request.user)
		return queryset

class ParserView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, "parser.html")

    def post(self, request):
        document = request.POST["content"]
        clippings = split_document(document)
        data = []
        with transaction.atomic():
            for clipping in clippings:
                name, author, page, position, date, content = extract_clipping(clipping)
                autor_object, _ = models.Autor.objects.get_or_create(nome=author)
                livro_object, _ = models.Livro.objects.get_or_create(titulo=name, autor=autor_object)
                recorte_object = models.Recorte.objects.create(
                    livro=livro_object, \
                    autor=autor_object, \
                    posicao=position, \
                    user=request.user, \
                    texto=content
                )
                data.append({
                    "livro": name,
                    "autor": author,
                    "posicao": position,
                    "texto": content
                })

        if request.is_ajax:
            return JsonResponse(data, safe=False)
        
