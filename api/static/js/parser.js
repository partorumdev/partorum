
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function sendDocument(file){
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = event => {
        console.log(event);
        $.ajax({
            url: "/parser_create/",
            type: "POST",
            data: {content: event.target.result},
            success: alert("Recortes importados com sucesso!")
        });
    }
}
document.getElementById('input-file').addEventListener('change',
    (event) => {
        event.preventDefault();
        console.log(event);
        input = event.target;
        if ('files' in input && input.files.length > 0) {
            sendDocument(input.files[0])
        }
    }
);
